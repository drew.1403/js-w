function miFuncion(params) {
    console.log('saludos desde miFuncion');
    
}

let anonimF = function () {
    console.log('saludos desde anonimF');
}

let arrowF = () => console.log('saludos desde arrowF');




function funcionCallback(){
    console.log('Despues de 4 segundos se ejecuta');
}

setTimeout(anonimF,5000);
setTimeout(arrowF,3000);
setTimeout(funcionCallback,4000);