let x = 2;       // Allowed

{
  let x = 3;   // Allowed
}

{
  let x = 4;   // Allowed
}

const PI = {nombre:'mbappi', valor:13.24};

PI.valor = 14;

console.log(PI);

//PI = {nombre:'dembow', valor:14};   ---->>> no permitido, puedes cambiar propiedades pero no cambiar la contante en genervarv 

var car = {
        nombre:'Fiat', 
        anio: '2017', 
        color:'verde',
        full_str: function (){
                    return this.nombre + " | " + this.anio + " | " + this.color;

                }

    };

console.log(car.full_str()); //invocando la funcion acompañada de () retorna 

let z, a=1;

z = a++;//Asigna y luego incremente
console.log(z);
console.log(a);


//Incrementa y luego asigna

z= ++a;
console.log(z);


let entrada = 'javascriptloops';

for (let index = 0; index < entrada.length; index++) {
    if (entrada[index] == 'a' || entrada[index] == 'e' || entrada[index] == 'i' || entrada[index] == 'o' || entrada[index] == 'u') {
        console.log(entrada[index]);
        
    }
   
    
}
for (let index = 0; index < entrada.length; index++) {
    if (entrada[index] == 'a' || entrada[index] == 'e' || entrada[index] == 'i' || entrada[index] == 'o' || entrada[index] == 'u') {
        continue;
        
    }
    console.log(entrada[index]);
   
    
}

let r = '3' , j = 3, p;

p = r === j;



console.log(p); 

// AND en JS =>  &&

// OR en JS => ||

if (r!==j || r > j) {
    console.log('Aqui pasa algo');
    
}


let minumero = '18'

let edad = Number(minumero);

let respuesta = edad >= 18 ? 'Mayor de Edad' : "Menor de edad";

console.log(respuesta);

let score = 
    
