class Orden{
    static contadorOrdenes = 0;
    get MAX_PRODUCTOS(){
        return 5;
    }
    constructor(){
        this._idOrden = ++Orden.contadorOrdenes;
        this._productos = [];
        this._contadorProductosAgregados = this._productos.length;

    }

    get getIdOrden(){
        return this._idOrden;
    }

    agregarProducto(new_producto){
        if(this._productos.length < Orden.MAX_PRODUCTOS){
            this._productos.push(new_producto);
        }
        else{
            console.log('No se pueden agregar más productos');
        }
        
    }
    
    calcularTotal(){
        var total = 0;

        for (let index = 0; index < this._productos.length; index++) {
            let precio = this._productos[index].getPrecio;
            total+=precio;
            
        }

        return total;
    }

    mostrarOrden(){
        var txt= `ORDEN --->> ID: ${this._idOrden}. PRODUCTOS:  \n\n`;
        for (let index = 0; index < this._productos.length; index++) {
            txt = txt + this._productos[index].toString() + '\n';
            
        }

        return txt;


    }


}


