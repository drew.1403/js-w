 let car = {
    nombre: 'volvo',
    marca: 'ford',
    get datos(){
        return this.nombre + ' ' + this.marca;
    },

    set setLang(new_lang){
        this.idioma = new_lang;
    }

 }

 car.color = 'verde';
 car.str = function (){
     return this.nombre + ' ' + this.marca + ' ' + this.color;
 };

 console.log(car);
 console.log(car.str());

 let txt='';

 for (let property in car) {
     txt+= property + ':' + car[property] + ' ';
     
 }
 console.log(txt);

 delete car.str;

 console.log(car);

 console.log(car.datos)

car['idioma'] = 'esp';

console.log(car);

car.setLang = 'EN';
console.log(car);

car.s;
console.log(car);



