class Monitor{

    static contadorMonitores = 0;
    constructor(marca, tamanio){
        this._idMonitor = ++Monitor.contadorMonitores;
        this._marca = marca;
        this._tamanio = tamanio;

    }

    get getIdMonitor(){
        return this._idMonitor;

    }

    get getMarca(){
        return this._marca;
    }

    set setMarca(new_marca){
        this._marca = new_marca;
    }

    get getTamanio(){
        return this._tamanio;
    }

    set setTamanio(new_tamanio){
        this._tamanio = new_tamanio;
    }

    toString(){
        return `ID MONITOR: ${this.getIdMonitor} | MARCA: ${this.getMarca} | TAMANIO: ${this.getTamanio}`;
    }
}