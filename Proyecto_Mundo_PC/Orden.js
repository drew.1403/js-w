class Orden{

    static contadorOrdenes=0;

    constructor(){
        this._idOrden = ++Orden.contadorOrdenes;
        this._computadoras = [];
    }

    agregarComputadoras(computadora){
        this._computadoras.push(computadora);
    }

    mostrarOrden(){
        var computadorasOrden= '';

        for (let computadora of this._computadoras) {
            computadorasOrden = computadorasOrden + `${computadora}` + '\n';
            
        }

        console.log(`Orden ID: ${this._idOrden}, Computadoras: \n ${computadorasOrden}`);
    }

    toString(){
        return `Orden ID: ${this._idOrden}`
    }

}

