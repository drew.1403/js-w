/*

PARA COMENTARIOS
DE VARIAS LINEAS 
JS

*/

//Tipo de dato String
var nombre = 'gory';

nombre = 20;
console.log(typeof nombre );


//Tipo de dato numerico
var numero = 1000;
console.log(numero);

//Tipo de dato object

var objeto = {
    nombre:'drew',  //Propiedades de objeto se usa la nomenclatura del punto para acceder a algun valor
    apellido : 'Márquez',
    edad:20
}

console.log(objeto);
console.log(objeto.nombre);

var bandera = true;
console.log(bandera);
console.log(typeof bandera);

//Tipo de dato function

function Funcion(){}

console.log(Funcion);

console.log(typeof Funcion);

//Tipo de dato Symbol

var simbolo = Symbol();
console.log(simbolo);
console.log(typeof simbolo);

//Tipo clase es una function

class Persona{
    constructor(nombre,apellido,edad){
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
    }
}

console.log(Persona);
console.log(typeof Persona); 

//Tipo de dato undefined

var x;
console.log(x);
console.log(typeof x);

//Tipo de dato null = ausencia de valor

var y = null;
console.log(y);
console.log(typeof y);

// Arrays

var materias = ['matematica','castellano','ingles'];

console.log(materias);
console.log(typeof materias);

