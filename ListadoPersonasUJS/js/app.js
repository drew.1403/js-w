const personas = [
    new Persona('Andres','Márquez'),
    new Persona('Javi','Martínez')

];



function mostrarPersonas(){
    console.log('Mostrar Personas... ');
    let txt='';

    for (const persona of personas) {
        console.log(persona);
        txt += `<li> ${persona.nombre} ${persona.apellido}</li>`;
        
    }

    document.getElementById('personas').innerHTML = txt;
}


function agregarPersona(){
    const forma = document.forms[0];

    let nombre = forma['nombre'];
    let apellido = forma['apellido'];

    if (nombre.value != '' && apellido.value != '') {
        const persona = new Persona(nombre.value,apellido.value);
        personas.push(persona); //agregamos la persona al array de personas
        mostrarPersonas(); 
        
    }
    else{
        console.log('No hay informacion para agregar');
    }

}