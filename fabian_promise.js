/*
function dividirArr(numerador, denominador, callback) {
    let respuesta = numerador.map( (val,index) =>{
        return val / denominador[index];
    });

    callback(respuesta);
    
}

const mostrar = (param) => {
    param.forEach(element => {
        console.log(`Elemnto:'+ ${element}`);
        
    });

}


 */


// PRONISE 

function dividir_arr(numerador,denominador){
    return new Promise((onSucces,onError)=>{

        setTimeout(()=>{
            if(numerador.length !== denominador.length){
                onError('arrays distintos tamaños');
            }

            const siCero = denominador.some(val => val === 0);

            if(siCero){
                onError('Denominador no puede ser 0');
            }

            let respuesta = numerador.map((val,index)=>{
                const div = val / denominador[index];
                return div;
            });

            onSucces(respuesta);
        },1500);



    });





}

const mostrar = (param) => {
    param.forEach(element => {
        console.log(`Elemnto:'+ ${element}`);
        
    });

}

dividir_arr([1,2,3],[4,0]).then(mostrar).catch((error)=>{
    console.log(error);
});
