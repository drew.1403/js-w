function findLongestWordLength(str) {
    
    let array_palabras = str.split(" ");

    let array_tam_palabras = array_palabras.map((palabra) => palabra.length);

    let mayor = 0;
    for (let index = 0; index < array_tam_palabras.length; index++) {
        if (array_tam_palabras[index] > mayor) {
            mayor = array_tam_palabras[index];
            
        }
        
    }

    return mayor;


}

console.log(findLongestWordLength("Google do a barrel roll"));
