function frankenSplice(arr1, arr2, n) {

    let inicio_cadena = arr2.slice(0,n);
    let fin_cadena = arr2.slice(n);
    let array_final = inicio_cadena.concat(arr1,fin_cadena);
    return array_final;
  }
  
  console.log(frankenSplice(["claw", "tentacle"], ["head", "shoulders", "knees", "toes"], 2));