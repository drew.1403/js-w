function largestOfFour(arr) {

    let mayoresPorArray = arr.map((array) => {

        let mayor=array[0];
        for (const element of array) {
            if (element > mayor) {
                mayor = element;
                
            }
            
        }

        return mayor;

    });
    return mayoresPorArray;
  }
  
  console.log(largestOfFour([[17, 23, 25, 12], [25, 7, 34, 48], [4, -10, 18, 21], [-72, -3, -17, -10]]));
  
  