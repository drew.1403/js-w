class Persona{

    static id=0; //atributo de clase / static attribute

    static get creencia(){  // constante estatica, es una simulacion a traves del retorno de una variable
      const creencia = 'Dios';
      return creencia;
    }

    constructor(name, age, ocupation){
        this._name = name;
        this._age = age;
        this._ocupation = ocupation;
        this._idPersona = ++Persona.id;
        console.log('Se incrementa el contador:' + Persona.id);

    }

    static atributos(object){
        for (let property in object) {
            console.log( property + ' : ' + object[property]);
            
        }

       
    }
    
}

let p1= new Persona('andres',20,'programmer');
let p2= new Persona('jason',12,'doctor');
let p3= new Persona('lili',41,'nurse');
let p4= new Persona('carlos',51,'football player');

Persona.atributos(p4);

console.log(Persona.creencia) ;







