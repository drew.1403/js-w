// Check to see if all elements in an array
// are even numbers.

function allEven(input) {
  return input.every(element => element % 2 === 0);
}

// Check to see if all elements in an array
// are of the same type.

function allSameType(input) {
  return input.every((element, index, array) => typeof element === typeof array[array.length -1]);
}

// Check to see if every element in the matrix is
// an array and that every element in the array is
// greater than 0.

function positiveMatrix(input) {
  return input.every(row => Array.isArray(row) && row.every(element => element >= 0) );
}

// Check that all items in an array are strings
// and that they all only contain the same vowels.

function allSameVowels(input) {
  return input.every( word => {

    let regexpVowels = /[aeiou]/g;
    let onlyVowels = word.match(regexpVowels);
    let vowel = onlyVowels[0];

    let allSameVowel = onlyVowels.every(element => element === vowel);

    return typeof word === 'string' && allSameVowel;
  });
}

module.exports = {
  allEven,
  allSameType,
  positiveMatrix,
  allSameVowels
};


