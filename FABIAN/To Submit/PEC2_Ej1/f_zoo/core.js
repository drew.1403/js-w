const { animals } = require('./data');
const data = require('./data');



function entryCalculator(entrants) {
  if (!entrants || Object.entries(entrants).length === 0) {
    return 0;

  }
  
  let array = Object.entries(entrants);
  
  let result = array.reduce((acumulador,element) => {
    let tipoPersona = element[0];
    let cantidadPersona = element[1];

    let result = data.prices[tipoPersona] * cantidadPersona;

    return acumulador + result;

  },0);

  return result;

}

function schedule(dayName) {
  // your code here
  const hours = data.hours;

  let scheduleReadable = Object.keys(hours).reduce((obj,day) => {
    if (day === 'Monday') {
        obj[day] = 'CLOSED';
        return obj; 
    }
    obj[day] = `Open from ${hours[day].open}am until ${hours[day].close - 12}pm`;
    return obj;
  },{});

  if (!dayName) return scheduleReadable;
  else{
    let obj={};
    obj[dayName] = scheduleReadable[dayName];
    return obj;
  } 

  
}

function animalCount(species) {
  // your code here
  const animals = data.animals;
  let animalsObject = animals.reduce( (obj, especie) => {
    let cantidadPorEspecie = especie.residents.length;
    obj[especie.name] = cantidadPorEspecie;
    return obj;

  },{});

  if (!species) return animalsObject;
  else{
    return animalsObject[species];
  }
}

function animalMap(options) {
  // your code here
  const animals = data.animals;
  if (!options) {
    return animals.reduce( (obj, item) => {
      obj[item.location] = animals.filter(especie => especie.location === item.location).map(element => element.name);
      return obj;
           
    },{} ); 
  }
  else if (Object.keys(options).length === 1) {
    if ('includeNames' in options) {
      return animals.reduce( (obj, item) => {
        obj[item.location] = animals.filter(especie => especie.location === item.location).map(element =>{
            let objeto = {};
            objeto[element.name] = element.residents.map(element => element.name);
            return objeto;
        });
        return obj;
             
      },{} );

    }
    else if ('sex' in options) {
      return animals.reduce( (obj, item) => {
        obj[item.location] = animals.filter(especie => especie.location === item.location).map(element => element.name);
        return obj;
             
      },{} );;
      
    }
    
  }
  else{
    
    return animals.reduce( (obj, item) => {
      obj[item.location] = animals.filter(especie => especie.location === item.location).map(element =>{
          let objeto = {};
          let nameForSex = element.residents.filter(element => element.sex === options.sex).map(element => element.name);
          
          objeto[element.name] = nameForSex;
          return objeto;
      });
      return obj;
           
    },{} ); 

    
  }
}

function animalPopularity(rating) {
  // your code here
  const animalsByPopularity = animals.reduce( (obj, item) => {
    obj[item.popularity] = animals.filter(especie => especie.popularity === item.popularity).map(element => element.name);
    return obj;      
  },{} );
  if (!rating) {
    return animalsByPopularity;  
  }
  else{
    return animalsByPopularity[rating];
  }
}

function animalsByIds(ids) {
  // your code here
  const animals = data.animals;
  if (!ids) {
    return [];

  }
  else if (Array.isArray(ids)) {
    return ids.map(id => animals.filter(element => element.id === id)[0]);
  }
  else{
    return animals.filter(element => element.id === ids);
  }
}

function animalByName(animalName) {
  // your code here
  if (!animalName) {
    return {};
    
  }
  else{
    let animalByName = animals.filter(element => element.residents.some(element=> element.name === animalName));
    let specieName = animalByName[0].name;
    let animal = animalByName[0].residents.filter(element => element.name === animalName);
    animal[0]['species'] = specieName;
    return animal[0];

  }


}

function employeesByIds(ids) {
  // your code here
  const employees = data.employees;
  if (!ids) {
    return [];

  }
  else if (Array.isArray(ids)) {
    return ids.map(id => employees.filter(element => element.id === id)[0]);
  }
  else{
    return employees.filter(element => element.id === ids);
  }
}

function employeeByName(employeeName) {
  // your code here
  if (!employeeName) {
    return {};
    
  }
  else{
    const employees = data.employees;
    let employeeByName = employees.filter(element => element.firstName === employeeName || element.lastName === employeeName);
    return employeeByName[0];

  }

}


function managersForEmployee(idOrName) {
  // your code here
  const employees = data.employees;
  
  let employeeFiltered = employees.filter(employee => employee.firstName === idOrName || employee.lastName === idOrName || employee.id === idOrName);
  
  if (employeeFiltered[0].managers.length != 0) {
      let managerName = employeeFiltered[0].managers.map(id => {
        let managerFiltered = employees.filter(employee => employee.firstName === id || employee.lastName === id || employee.id === id);
        let nombre = managerFiltered[0].firstName;
        let apellido = managerFiltered[0].lastName;
        return nombre + ' ' + apellido;
  
    
      });
      
      employeeFiltered[0].managers = managerName;
      return employeeFiltered[0];
    
  }
  
  return employeeFiltered[0];
  

}

function employeeCoverage(idOrName) {
  // your code here
  const animals = data.animals;
  const employees = data.employees;
  function employeesCoverage(idOrName){
  
    let employeeFiltered = employees.filter(employee => employee.firstName === idOrName || employee.lastName === idOrName || employee.id === idOrName);
    let employeeName = employeeFiltered[0].firstName + ' ' + employeeFiltered[0].lastName;
    //console.log(employeeName);
    let animalsForEmployees = employeeFiltered[0].responsibleFor.map(id => {
      let animal = animals.filter(animal => animal.id === id);
      let nameAnimal = animal[0].name;
      return nameAnimal;
    
    });
    let obj ={};
    obj[employeeName] = animalsForEmployees;
    return obj;
  }
  if (!idOrName) {
    
    return employees.reduce((acumulador,element) => {

      let employeeName = Object.keys(employeesCoverage(element.id))[0];
      let coverage = Object.values(employeesCoverage(element.id))[0];
      acumulador[employeeName] = coverage;
      return acumulador;
    }, {} );
    
  }
  else{
    return employeesCoverage(idOrName);
  }

}


module.exports = {
  entryCalculator,
  schedule,
  animalCount,
  animalMap,
  animalPopularity,
  animalsByIds,
  animalByName,
  employeesByIds,
  employeeByName,
  managersForEmployee,
  employeeCoverage
};
