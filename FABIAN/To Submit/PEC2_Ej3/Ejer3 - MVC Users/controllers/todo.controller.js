/**
 * @class Controller
 *
 * Links the user input and the view output.
 *
 * @param model
 * @param view
 */
class TodoController {
    constructor(service, view) {
      this.service = service;
      this.view = view;
  
      // Explicit this binding
      this.service.bindTodoListChanged(this.onTodoListChanged);
      this.view.bindAddTodo(this.handleAddTodo);
      //this.view.bindEditTodo(this.handleEditTodo);
      this.view.bindDeleteTodo(this.handleDeleteTodo);
      this.view.bindDeleteByCheckbox(this.handleDeleteByCheckbox);
      this.view.bindToggleTodo(this.handleToggleTodo);
      this.view.bindToggleAll(this.handleToggleAll);

      this.view.bindEditAll(this.search,this.handleEditTodo);
    
      // Display initial todos
      this.onTodoListChanged(this.service.users);
    }
  
    onTodoListChanged = users => {
      this.view.displayTodos(users);
    };
  
    handleAddTodo = todoText => {
      this.service.addTodo(todoText);
    };
  
    handleEditTodo = (id, userEdited) => {
      this.service.editTodo(id, userEdited);
    };
  
    handleDeleteTodo = id => {
      this.service.deleteTodo(id);
    };
  
    handleToggleTodo = id => {
      this.service.toggleTodo(id);
    };

    handleToggleAll = option => {
      this.service.toggleAll(option);
    };

    handleDeleteByCheckbox = () => {
      this.service.deleteByCheckbox();
    };

    search = id => this.service.searchUser(id);
  }
  