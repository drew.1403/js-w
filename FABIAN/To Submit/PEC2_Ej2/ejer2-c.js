const findOne = (list, { key, value }) => { 
    return new Promise((resolve,reject) => {
        setTimeout(() => {
            const element = list.find(element => element[key] === value);
            element ? resolve(element) : reject({ msg: 'ERROR: Element Not Found' });
        }, 2000);
    
    });
    
        
};
    
const users = [
    {
        name: 'Carlos',
        rol: 'Teacher'
    },
    {
        name: 'Ana',
        rol: 'Boss'
    }
];



async function resultSearch(list, { key, value }){ //Se define la funcion asincrona
    // se usará try/catch para el manejo del resultado de la promesa
    try {
        const resultFind = await findOne(list, { key, value });// await se encarga de esperar el resultado de la promesa.
        console.log(`user: ${resultFind['name']}`);//en caso de ser una promesa resuelta imprime el resultado de la promesa
        
    } catch (error) { // En caso de promesa rechazada
        console.log(error['msg']); // imprime resultado de reject(Mensaje de error)
        
    }
    
    
}

resultSearch(users,{ key: 'name', value: 'Mbappe' });