const findOne = (list, { key, value }) => { // findOne no hará uso de calback
// En su lugar findOne retornará una Promesa que simula una operacion asincrona con setTimeout
    return new Promise((resolve,reject) => {// las funciones resolve, reject, al ser llamadas resuelven o rechazan (en caso de error)la promesa,respectivamente.
      setTimeout(() => {
        const element = list.find(element => element[key] === value);
        //En caso de encontrarse el usuario, la promesa se resuelve con la funcion resolve
        element ? resolve(element) : reject({ msg: 'ERROR: Element Not Found' });
        //En caso de no encontrarse el usuario la promesa se rechaza con la funcion reject
      }, 2000);

    });

    
};

const users = [
  {
    name: 'Carlos',
      rol: 'Teacher'
    },
    {
      name: 'Ana',
      rol: 'Boss'
    }
  ];
  
  findOne(users, { key: 'name', value: 'JR' })
    .then(({ name }) => console.log(`user: ${name}`))// En este bloque then especifica que funcion realizara resolve en el interior de la promesa(promesa resuelta)
    .catch(({ msg }) => console.log(msg)); // catch sera la accion que tomara reject en caso de un error de busqueda(promesa rechazada)
  

  