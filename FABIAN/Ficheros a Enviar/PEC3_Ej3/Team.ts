class Team {
    private shortName : string;
    private longName : string;
    private imgSrc : string;
    private president : string;
    private sponsor : string;
    private members : number;
    private budget : number = 0;
    private founded : number;
    players : Player[];
    league : League;

    constructor(shortName : string, longName : string, imageSrc : string, 
                president : string, sponsor : string, members : number, budget : number,
                founded : number, players: Player[], league : League
    ){
        this.shortName = shortName;
        this.longName = longName;
        this.imgSrc = imageSrc;
        this.president = president;
        this.sponsor = sponsor;
        this.members = members;
        this.budget = budget;
        this.founded = founded;
        this.players = players;
        this.league = league;
        
    }

    getShortName() : string{
        return {} as string;

    }

    setShortName(shortName : string){

    }

    getLongName() : string{
        return {} as string;

    }

    setLongName(longName : string){

    }

    getImageSrc() : string{
        return {} as string;

    }

    setImageSrc(imageSrc : string){

    }

    getPresident() : string{
        return {} as string;

    }

    setPresident(president : string){

    }

    getSponsor() : string{
        return {} as string;

    }

    setSponsor(sponsor : string){

    }

    getMembers() : number{
        return {} as number;

    }

    setMembers(Members : number){

    }

    getBudget() : number{
        return {} as number;

    }

    setBudget(Budget : number){

    }

    getFounded() : number{
        return {} as number;

    }

    setFounded(Founded : number){

    }

    addPlayer(player : Player){

    }

    removePlayer(player : Player){
        
    }

    getSquad() : Player[]{
        return {} as Player[];

    }

    setSquad(Squad : Player[]){

    }

    getLineUp() : Player[]{

        return {} as Player[];
    

    }

    getPlayerByNumber(number : number) : Player{
        return {} as Player;
    }

    getLeague() : League{
        return {} as League;

    }

    setLeague(League : League){

    }

    toString(): string{
        return {} as string;

    }
    
}