/**
 * @class View
 *
 * Visual representation of the model.
 */
import {Todo,todo} from "../models/todo.model"

export class TodoView {
    app : HTMLDivElement;
    form : HTMLFormElement;
    input : HTMLInputElement;
    submitButton : HTMLButtonElement;
    title : HTMLHeadingElement;
    todoList : HTMLUListElement;
    _temporaryTodoText : string;
    constructor() {
      this.app = this.getElement("#root") as HTMLDivElement;
      this.form = this.createElement("form") as HTMLFormElement;
      this.input = this.createElement("input") as HTMLInputElement;
      this.input.type = "text";
      this.input.placeholder = "Add todo";
      this.input.name = "todo";
      this.submitButton = this.createElement("button") as HTMLButtonElement;
      this.submitButton.textContent = "Submit";
      this.form.append(this.input, this.submitButton);
      this.title = this.createElement("h1") as HTMLHeadingElement;
      this.title.textContent = "Todos";
      this.todoList = this.createElement("ul", "todo-list") as HTMLUListElement;
      this.app.append(this.title, this.form, this.todoList);
  
      this._temporaryTodoText = "";
      this._initLocalListeners();
    }
  
    get todoText() : string{
      return this.input.value;
    }
  
    _resetInput(): void {
      this.input.value = "";
    }
  
    createElement(tag : string, className ?: string) : HTMLElement{
      const element = document.createElement(tag);
  
      if (className) element.classList.add(className);
  
      return element;
    }
  
    getElement(selector:string) : HTMLElement{
      const element = document.querySelector(selector) as HTMLElement;
  
      return element;
    }
  
    displayTodos(todos: Todo[]) {
      // Delete all nodes
      while (this.todoList.firstChild) {
        this.todoList.removeChild(this.todoList.firstChild);
      }
  
      // Show default message
      if (todos.length === 0) {
        const p = this.createElement("p") as HTMLParagraphElement;
        p.textContent = "Nothing to do! Add a task?";
        this.todoList.append(p);
      } else {
        // Create nodes
        todos.forEach(todo => {
          const li = this.createElement("li") as HTMLUListElement;
          li.id = todo.id;
  
          const checkbox: HTMLInputElement = <HTMLInputElement>this.createElement("input");
          checkbox.type = "checkbox";
          checkbox.checked = todo.complete;
  
          const span : HTMLSpanElement = <HTMLSpanElement>this.createElement("span");
          span.contentEditable = "true";
          span.classList.add("editable");
  
          if (todo.complete) {
            const strike : HTMLSpanElement= <HTMLSpanElement>this.createElement("s");
            strike.textContent = todo.text;
            span.append(strike);
          } else {
            span.textContent = todo.text;
          }
  
          const deleteButton = <HTMLButtonElement>this.createElement("button", "delete");
          deleteButton.textContent = "Delete";
          li.append(checkbox, span, deleteButton);
  
          // Append nodes
          this.todoList.append(li);
        });
      }
  
      // Debugging
      
    }
  
    _initLocalListeners() {
      this.todoList.addEventListener("input", (event: Event) => {
        if ((event.target as HTMLSpanElement).className === "editable") {
          this._temporaryTodoText = (event.target as HTMLSpanElement).innerText;
        }
      });
    }
  
    bindAddTodo(handler: Function) {
      this.form.addEventListener("submit", (event : Event) => {
        event.preventDefault();
  
        if (this.todoText) {
          handler(this.todoText);
          this._resetInput();
        }
      });
    }
  
    bindDeleteTodo(handler : Function) {
      this.todoList.addEventListener("click", (event : Event) => {
        if ((event.target as HTMLButtonElement).className === "delete") {
          const id = (event.target as HTMLButtonElement).parentElement?.id;
          

  
          handler(id);
        }
      });
    }
  
    bindEditTodo(handler : Function) {
      this.todoList.addEventListener("focusout", (event : Event) => {
        if (this._temporaryTodoText) {
          const id : string = (event.target as HTMLSpanElement).parentElement!.id;
  
          handler(id, this._temporaryTodoText);
          this._temporaryTodoText = "";
        }
      });
    }
  
    bindToggleTodo(handler : Function) {
      this.todoList.addEventListener("change", (event : Event) => {
        if ((event.target as HTMLInputElement).type === "checkbox") {
          const id = (event.target as HTMLInputElement).parentElement!.id;
  
          handler(id);
        }
      });
    }
  }
  